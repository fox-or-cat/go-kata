package main

import (
	"errors"
	"fmt"
)

func main() {
	fmt.Println(ExecuteMergeDictsJob(&MergeDictsJob{Dicts: []map[string]string{{"ето": "или ето"}, {"b": "c"}}}))
}

type MergeDictsJob struct {
	Dicts       []map[string]string
	Merged      map[string]string
	isFinishead bool
}

var (
	errNotEnoughDicts = errors.New("at least 2 dictionaries are required")
	errNilDict        = errors.New("nil dictionary")
)

func (j *MergeDictsJob) setFinishead() {
	j.isFinishead = true
}

func ExecuteMergeDictsJob(job *MergeDictsJob) (*MergeDictsJob, error) {
	defer job.setFinishead()
	if len(job.Dicts) < 2 {
		return job, errNotEnoughDicts
	}
	job.Merged = make(map[string]string)
	for _, dict := range job.Dicts {
		if dict == nil {
			return job, errNilDict
		}
		for c, v := range dict {
			job.Merged[c] = v
		}
	}
	return job, nil
}
