package main

import (
	"fmt"
)

type Project struct {
	Name  string
	Stars int
}

func main() {
	projects := []Project{
		{
			Name:  "https://github.com/docker/compose",
			Stars: 27600,
		},
		{
			Name:  "https://github.com/ossu/computer-science",
			Stars: 135262,
		},
		{
			Name:  "https://github.com/slidevjs/slidev",
			Stars: 25457,
		},
		{
			Name:  "https://github.com/jlevy/the-art-of-command-line",
			Stars: 130123,
		},
		{
			Name:  "https://github.com/python-telegram-bot/python-telegram-bot",
			Stars: 21116,
		},
		{
			Name:  "https://github.com/twbs/bootstrap",
			Stars: 161970,
		},
		{
			Name:  "https://github.com/animate-css/animate.css",
			Stars: 77109,
		},
		{
			Name:  "https://github.com/sindresorhus/awesome",
			Stars: 239944,
		},
		{
			Name:  "https://github.com/getify/You-Dont-Know-JS",
			Stars: 165127,
		},
		{
			Name:  "https://github.com/ohmyzsh/ohmyzsh",
			Stars: 155649,
		},
		{
			Name:  "https://github.com/EbookFoundation/free-programming-books",
			Stars: 268191,
		},
		{
			Name:  "https://github.com/chubin/cheat.sh",
			Stars: 34733,
		},
		{
			Name:  "https://github.com/jwasham/coding-interview-university",
			Stars: 248840,
		},
	}
	m := map[string]Project{}

	// в цикле запишите в map
	for _, value := range projects {
		m[value.Name] = value
	}
	// в цикле пройдитесь по мапе и выведите значения в консоль
	for _, value := range m {
		fmt.Println(value)
	}
}
